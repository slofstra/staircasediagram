/*  Reads in a graph from cin, in the following format:

    n m 
    a1 b1
    ...
    am bm

    where 
    - n is the number of vertices, 
    - m is the number of edges, and
    - ai bi indicates an edge between vertices ai and bi

    Vertices are identified by integers between 0 and n-1. 

    Outputs the number of staircase diagrams on the graph.
*/ 

#include <iostream>
#include "staircase.h"
using namespace std;

int main(void)
{
    Graph G = Graph::read();

    StaircaseDiagrams SD(G); 

    cout << "There are " << SD.count() << " staircase diagrams on this graph"
         << endl;

//    cout << "There are " << SD.count_connected() << " connected staircase diagrams on this graph"
//         << endl;

    cout << "There are " << SD.count_fully_supported() << " fully supported staircase diagrams on this graph"
         << endl;
    return 0;
}
    
