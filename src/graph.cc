#include "graph.h"
#include <stdexcept>
#include <queue>
#include <vector>
#include <iostream>

void Graph::add_edge (unsigned a, unsigned b) 
{   
    if (a >= num_vertices() || b >= num_vertices()) {
        throw std::range_error("Edge index out of range");
    }

    edges[a][b] = 1;
    edges[b][a] = 1;
}

Graph::vertex_set Graph::neighbours (const vertex_set &S) 
{    
    vertex_set acc = empty_set();

    // find all the vertices adjacent to all the elements of S
    for (int i=0; i<num_vertices(); i++) 
        if (S[i]) acc |= edges[i];
        
    return acc;
}

std::list<Graph::vertex_set> Graph::connected_components(const vertex_set &S) 
{
    std::list<vertex_set> out; 
    vertex_set marked = empty_set();

    for (int i=0; i<num_vertices(); i++) {
        if (S[i] && !marked[i]) {
            vertex_set new_comp = empty_set();
            std::queue<int> search; 
            search.push(i);
            while (!search.empty()) {
                int k = search.front(); search.pop();
                // if we haven't been here before, add it to the component, and mark the vertex
                if (S[k] && !marked[k]) { 
                    new_comp[k]=1;  
                    marked[k]=1;

                    // add the adjacent vertices to k
                    int j = edges[k].find_first();
                    while (j != vertex_set::npos) {
                        search.push(j);
                        j = edges[k].find_next(j);
                    }
                }
            }
            
            // add the new component --- this should make a copy
            out.push_back(new_comp);
        }
    }

    return out;
}

// Every connected subgraph has a unique enumeration v_1,...,v_n, where
// v_1 is the smallest vertex, and v_i is the smallest vertex of v_i,...,v_n
// which is connected to v_1,...,v_{i-1}. 
// We list all possible enumerations of this form using a depth-first search,
// as per David Eisenstat's answer at 
// http://stackoverflow.com/questions/15658245/efficiently-find-all-connected-induced-subgraphs
std::list<Graph::vertex_set> Graph::connected_subgraphs(const vertex_set &S) 
{
    std::list<Graph::vertex_set> out;


    // for each element s of S, find the enumerations starting with s. 
    // only allow vertices larger than this starting vertex 
    // starting_vertices will be decreased and increased during the recursion
    vertex_set starting_vertices = S;
    for (int i = S.find_first(); i != vertex_set::npos; i = S.find_next(i)) {
        // construct the singleton containing i
        vertex_set starting_graph = empty_set();
        starting_graph[i] = 1;

        // remove i from starting set
        starting_vertices[i] = 0;

        // now recursively construct all sequences starting with i
        connected_subgraphs_helper(starting_graph, starting_vertices, edges[i], out);
    }
    
    return out;
}

// At each step, SG is the subgraph constructed up to this point, and
// not_considered is the set of vertices not yet considered, and N consists of
// all neighbours of SG
// Output is placed into out.
void Graph::connected_subgraphs_helper(const vertex_set &SG, vertex_set &not_considered, 
        const vertex_set &N, std::list<Graph::vertex_set> &out)
{
    // find the smallest vertex not yet considered which is adjacent to SG, if any exists
    int i = (not_considered & N).find_first();

    // if we don't find any such vertices, then we're at the end of the branch
    // this is when we add the set SG
    if (i == vertex_set::npos) { 
        out.push_back(SG);
    }
    else {
        // remove i from considered vertices        
        not_considered[i]=0;

        // first branch: we ignore this vertex
        connected_subgraphs_helper(SG, not_considered, N, out);

        // second branch: we add this vertex, and update the neighbour set
        vertex_set newS = SG;
        newS[i] = 1;
        vertex_set newN = N | edges[i];
        connected_subgraphs_helper(newS, not_considered, newN, out);
        
        // put i back into consideration for other branches of the tree 
        not_considered[i]=1;
    }
}

Graph Graph::read ()
{
    int n,m;
    std::cin >> n;
    std::cin >> m;
    Graph G(n);
    for (int i=0; i<m; i++) {
        int a,b;
        std::cin >> a;
        std::cin >> b;
        G.add_edge(a,b);
    }
    return G;
}

bool Graph::is_connected(const vertex_set &SG)
{
    // consider the empty set connected
    return (connected_components(SG).size() <= 1);
}

