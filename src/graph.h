/* Adjacency matrix representation of graph. For our purposes, graphs tend to 
   be small, so we don't do anything fancy. Vertices are always represented
   by numbers from 0,...,n-1. Sets of vertices are represented by the type
   vector_set, which is a dynamic_bitset. 
*/

#include <vector>
#include <list>
#include <boost/dynamic_bitset.hpp>

class Graph {
    public:
    /********************* Public types ***********************************/
        typedef boost::dynamic_bitset<> vertex_set;


    private: 
    /********************* Private data members ***************************/
        std::vector<vertex_set> edges;

    /********************* Private helper functions ***********************/

        // helper for connected subgraphs
        void connected_subgraphs_helper(const vertex_set &, vertex_set &, const vertex_set &, std::list<Graph::vertex_set> &);

    public: 
    /********************* Public function members ************************/

        // Constructor - n is the number of vertices (cannot be changed)
        Graph (unsigned n) : edges(n, vertex_set(n)) {}
    
        // Add an undirected edge from a to b
        void add_edge (unsigned a, unsigned b); 

        // Return the number of vertices
        unsigned num_vertices() { return edges.size(); }

        // Return an empty vertex set
        vertex_set empty_set() { return vertex_set(num_vertices()); }

        // All vertices which are adjacent to some element of the argument
        vertex_set neighbours (const vertex_set &);

        // The connected components of the subgraph induced by argument
        std::list<vertex_set> connected_components(const vertex_set &);

        // The non-empty connected subgraphs (specified by vertex set) of the
        // subgraph induced by the argument
        std::list<vertex_set> connected_subgraphs(const vertex_set &);

        // Return whether the subgrpah specified by the vertex set is connected
        bool is_connected(const vertex_set &);

    /******************* Static utility functions ************************/

        // Read in a graph from cin
        // Format should be 
        // n m a1 b1 ... am bm
        // where n = number of vertices
        //       m = number of edges
        //       ai bi = ith edge 
        static Graph read();
};
