#include "staircase.h"
// #include <iostream> // DEBUG

StaircaseDiagrams::Diagram StaircaseDiagrams::make_successor(const Diagram &D, const Graph::vertex_set &B) 
{
    // find the closed neighbourhood of B
    Graph::vertex_set BN = B | G.neighbours(B);

    // remove any descent which are in the open neighbourhood of B, and add in B
    Diagram S(D.support | B, (D.descents & ~(BN ^ B)) | B, D.size+1, &D);

    for (std::list<Graph::vertex_set>::const_iterator it = D.maximal_blocks.begin();
                it != D.maximal_blocks.end(); it++) {
    
        // add all blocks which do not touch B
        if (!it->intersects(BN)) S.maximal_blocks.push_back(*it);

    }

//    std::cout << "Made a block with support = " << S.support << ", descents = " << S.descents << ", size = " << S.size << std::endl; // DEBUG

    // make B a maximal block
    S.maximal_blocks.push_back(B);

    return S;
}

bool StaircaseDiagrams::is_valid_successor (const Diagram &D, const Graph::vertex_set &B)
{
    // A non-empty connected subgraph B is a valid successor to D if and only
    // if B satisfies the following properties: 
    // 1. is not contained in the support of D,
    // 2. does not contain any non-descents in the support of D,
    // 3. does not contain a maximal block, and
    // 4. touches every maximal block which does not have an element less than
    //    all the elements in B.
    // Note: two blocks touch if they either meet, or are adjacent. 

    // Important: we assume that B is connected and satisfies #2

    if (B.is_subset_of(D.support)) return false;
    
    // Don't check #2
    // if (B.intersects(D.support & (~D.descents))) return false;

    // the closed neighbourhood of B
    Graph::vertex_set BN = B | G.neighbours(B);

    for (std::list<Graph::vertex_set>::const_iterator it = D.maximal_blocks.begin();
                it != D.maximal_blocks.end(); it++) {

        if (it->is_subset_of(B)) return false;

        // B does not touch *it if and only if *it does not meet the closed neighbourhood of B
        if (!it->intersects(BN) && B.find_first() < it->find_first()) return false;
    }

    return true;
}

void StaircaseDiagrams::generate_successors (const Diagram &D, std::list<Diagram> &out)
{
    // generate all connected subgraphs not containing any non-descents in the support of D 
    std::list<Graph::vertex_set> BL = G.connected_subgraphs(~(D.support & (~D.descents)));

    // add valid successors to out
    for (std::list<Graph::vertex_set>::iterator it = BL.begin(); it != BL.end(); it++) {
        if (is_valid_successor(D, *it)) { 
            out.push_back(make_successor(D,*it));
        }
    }
}

void StaircaseDiagrams::generate_next_size ()
{
    int prev_size = diagrams_by_size.size()-1;

    diagrams_by_size.push_back(std::list<Diagram>());

    for (std::list<Diagram>::iterator it = diagrams_by_size[prev_size].begin(); 
            it != diagrams_by_size[prev_size].end(); it++) {
        generate_successors(*it, diagrams_by_size.back());
    }
}

unsigned StaircaseDiagrams::count ()
{
    while (!found_all_diagrams()) {
        generate_next_size();
    }

    int acc=0;
    for (int i=0; i<diagrams_by_size.size(); i++) acc+=diagrams_by_size[i].size();

    return acc;
}

unsigned StaircaseDiagrams::count_connected()
{
    while (!found_all_diagrams()) {
        generate_next_size();
    }

    int acc=0;
    for (int i=0; i<diagrams_by_size.size(); i++) {
        for (std::list<Diagram>::const_iterator iter = diagrams_by_size[i].cbegin();
                iter != diagrams_by_size[i].cend(); iter++) {
            if (G.is_connected(iter->support)) acc++;
        }
    }
    return acc;
}

unsigned StaircaseDiagrams::count_fully_supported()
{
    while (!found_all_diagrams()) {
        generate_next_size();
    }

    int acc=0;
    for (int i=0; i<diagrams_by_size.size(); i++) {
        for (std::list<Diagram>::const_iterator iter = diagrams_by_size[i].cbegin();
                iter != diagrams_by_size[i].cend(); iter++) {
            if (iter->support.all()) acc++;
        }
    }
    return acc;
}
