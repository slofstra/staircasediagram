#include <iostream>
#include <ostream>
#include <list>
#include "graph.h"
using namespace std;

int main (void) 
{
    Graph G = Graph::read();

    list<Graph::vertex_set> L = G.connected_subgraphs(~G.empty_set());

    cout << "Connected subgraphs:" << endl;
    for (list<Graph::vertex_set>::iterator S = L.begin(); S != L.end(); S++) {
        cout << *S << endl;
    }

    return 0;
}
