/* Contains classes for staircase diagrams. 

   Recall that a staircase diagram is a partially ordered lists of blocks,
   where each block is a connected subgraph of the underlying graph. A key
   feature of staircase diagrams is that the set of blocks containing a 
   given vertex forms a chain.

   Suppose the vertices of the underlying graph are ordered. By the property
   mentioned above, every staircase diagram  has a unique linearization
   B_1,...,B_n such that, for every k, B_k contains the smallest vertex among
   all vertices contained in the minimal elements of B_k,...,B_n. We identify
   staircase diagrams with this linearization. With this convention, every 
   staircase diagram B_1,...,B_n has a unique predecessor B_1,...,B_{n-1}.

   This file defines two main classes:

   1. A class representing the set of staircase diagrams on a given graph. This
      class includes methods for generating all staircase diagrams. 

   2. A nested class representing individual staircase diagrams. This class
      tracks the following data:
     
      - the support of the diagram
      - the maximal blocks of the diagram
      - the "top" or "left" descent set of the diagram 
      - the number of blocks in the diagram, and
      - a pointer to the predecessor staircase diagram.
*/

#include <list>
#include <vector>
//#include <iostream>
#include "graph.h"

class StaircaseDiagrams {

    public: 
        /**************** Public data members ***********************************/

        // Should not be constructed explicitly, get Diagrams from a StaircaseDiagram
        // instance
        class Diagram {
            public: 
                Graph::vertex_set support; // the support set
                Graph::vertex_set descents; // the descent set
                std::list<Graph::vertex_set> maximal_blocks; // a list of maximal blocks
                unsigned size; // number of blocks in the diagram

                // pointer to the predecessor diagram
                // should be null iff size==0
                const Diagram * pred;  

                // Basic constructor --- sets maximal_blocks to empty, so that should be
                // initialized manually if needed
                Diagram(const Graph::vertex_set s, const Graph::vertex_set d, unsigned sz, const Diagram *p) 
                            : support(s), descents(d), size(sz), maximal_blocks(), pred(p) { }
        };

    private: 
        /**************** Private data members ***************************/

        // the underlying graph
        Graph G; 
    
        // for each size, a list of diagrams of that size
        std::vector<std::list<Diagram>> diagrams_by_size; 
 
        /**************** Private function members ***********************/

        // determine whether a given block B can be the successor to a given
        // diagram D, assuming that B is connected and does not contain any
        // non-descents in the support of D
        bool is_valid_successor (const Diagram &, const Graph::vertex_set &);

        // make a new diagram by adding a block
        Diagram make_successor (const Diagram &, const Graph::vertex_set &);
    
        // Generate the successors of the given diagram, and place them 
        // (using push_back) into a supplied list. 
        void generate_successors (const Diagram &, std::list<Diagram> &);

        // Generate all staircase diagrams of the next size, placing into diagrams_by_size
        void generate_next_size ();

        // determine whether we have found all staircase diagrams on the underlying graph
        bool found_all_diagrams () { return diagrams_by_size.back().empty(); }

    public:
        /**************** Constructors ***********************************/ 
        StaircaseDiagrams (Graph GG) : G(GG), diagrams_by_size(1) { 
            diagrams_by_size[0].push_back(Diagram(G.empty_set(), G.empty_set(), 0, NULL)); // add the empty diagram
        }

        /**************** Public data members ****************************/

        // return the number of staircase diagrams on G
        unsigned count();  

        // return the number of connected staircase diagrams on G
        unsigned count_connected();

        // return the number of fully supported staircase diagrams on G
        unsigned count_fully_supported();
};

