A program to generate staircase diagrams over an arbitrary graph, in support
of the papers

- Richmond and Slofstra.  Staircase diagrams and enumeration of smooth Schubert
  varieties. arXiv:1510.06060

- Richmond and Slofstra. Smooth Schubert varieties in the affine flag variety
  of type $\tilde{A}$. arXiv:1702.02236

How to use: 

0. Make sure that gcc and make are installed

1. Go into the src/ subdirectory, and type 'make'

2. Move the count_staircases executable into the main directory

3. To count the number of staircase diagrams in, say, type A5, do

    ./count_staircases < input/A5

   Other input files are provided in the input directory. For an 
   arbitrary graph, run

    ./count_staircases

   and input the graph on standard input in the following format:

    n m
    a1 b1
    ...
    am bm

   where 'n' is the number of vertices, 'm' is the number of edges, and 'ai bi'
   indicates an edge between vertex 'ai' and vertex 'bi'. Vertices are
   specified as integers between 0 and n-1. Newlines are ignored, so the
   formatting is arbitrary. 

Notes:

Currently the program doesn't do anything other than count the number of
staircase diagrams. However, internally it does generate all staircase
diagrams. If you're interested in seeing more detailed output, please let me
know. 

The program does not generate labelled staircase diagrams. This means that it
cannot be used to enumerate smooth Schubert varieties in non-simply-laced types.

The program also does not generate spherical staircase diagrams (although this
would be easier to add). In affine type A, there is always one non-spherical
diagram, so the number returned by count_staircases is one more than the number
of smooth Schubert varieties. 

